# backcasting2050

西暦2050年までの主だった起きた出来事、起きそうな出来事を集めて、全体像を把握できるようにします。

このプロジェクトは不完全です。多くの情報を求めています。

わたし達がまだ知らない、重要と思われる事実があれば、[こちらへ投稿して知らせてください](https://bitbucket.org/tomk79/backcasting2050/issues?status=new&status=open) 。



## ライセンス - License

Creative Commons

[![クリエイティブ・コモンズ・ライセンス](https://i.creativecommons.org/l/by/2.1/jp/88x31.png)](http://creativecommons.org/licenses/by/2.1/jp/)

この 作品 は [クリエイティブ・コモンズ 表示 2.1 日本 ライセンスの下に提供されています](http://creativecommons.org/licenses/by/2.1/jp/)。

## 作者 - Author

- (C)Tomoya Koyanagi <tomk79@gmail.com>
- website: <http://www.pxt.jp/>
- Twitter: @tomk79 <http://twitter.com/tomk79/>


